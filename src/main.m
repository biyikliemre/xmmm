% XMMMMv1

clc, clear all, close all
delete *.asv

% globals

global C CSYM D E EF F FF FW IN M MASS NN PSMP SF sim TYP V X X0

% input

sim.name = 'e_D2crackXMMM';
fprintf('simulation %s\n',sim.name);
fprintf('----------------\n',sim.name);

% read input file from ../input/

fin1 = ['../input/' sim.name '.in'];
fid1 = fopen(fin1);

% run line-by-line

stop = 0;
while(~stop)
    
    % key
    
    line = fgetl(fid1);
    if(isempty(line)); continue; end;
    if(line(1)=='#'); continue; end;
    if(line==-1); stop = 1; continue; end;
    blanks = strfind(line,' ');
    key = line(1:blanks(1)-1);
    
    % dim
    
    if(strcmp(key,'dim')==1)
        s1 = line(blanks(1)+1:end);
        sim.dim = str2double(s1);
        fprintf('dimension is set to %.0f\n',sim.dim);
        if(sim.dim==1 || sim.dim==2)
            vi = [0 0 1];
        end
    end
    
    % fix
    
    if(strcmp(key,'fix')==1)        
        s1 = line(blanks(1)+1:blanks(2)-1);
        sel = str2double(s1);
        dof = line(blanks(2)+1:end);
        t1 = zeros(1,3);
        if(~isempty(strfind(dof,'x'))); t1(1) = 1; end;
        if(~isempty(strfind(dof,'y'))); t1(2) = 1; end;
        if(~isempty(strfind(dof,'z'))); t1(3) = 1; end;
        t2 = S(1,sel);
        t3 = S(2:1+t2,sel);
        FF(t3,1) = 1;
        FF(t3,2:4) = repmat(t1,t2,1); 
        fprintf('fix is applied to selection %.0f\n',sel); 
    end
    
    % frc
    
    if(strcmp(key,'frc')==1)        
        s1 = line(blanks(1)+1:blanks(2)-1);
        sel = str2double(s1);
        s1 = line(blanks(2)+1:end);
        vec = str2num(s1);
        t1 = S(1,sel);
        t2 = S(2:1+t1,sel);
        FF(t2,1) = 2;
        FF(t2,2:4) = repmat(vec,t1,1);  
        fprintf('force is applied to selection %.0f\n',sel); 
    end
        
    % io
    
    if(strcmp(key,'io')==1)
        s1 = line(blanks(1)+1:end);
        sim.ioUfreq = str2double(s1);
        fprintf('input/output update frequency is set to %.0f\n',sim.ioUfreq); 
    end
        
    % isDyn
    
    if(strcmp(key,'isDyn')==1)
        s1 = line(blanks(1)+1:end);
        sim.isDyn = str2double(s1);
        if(sim.isDyn)
            fprintf('dynamics is on\n');
        end
    end
    
    % isMMM
    
    if(strcmp(key,'isMMM')==1)
        s1 = line(blanks(1)+1:end);
        sim.isMMM = str2double(s1);
        if(sim.isMMM)
            fprintf('MMM is on\n');
        end
    end
    
    % isXMMM
    
    if(strcmp(key,'isXMMM')==1)
        s1 = line(blanks(1)+1:end);
        sim.isXMMM = str2double(s1);
        if(sim.isXMMM)
            fprintf('XMMM is on\n');
        end
    end
        
    % mass
    
    if(strcmp(key,'mass')==1)
        s1 = line(blanks(1)+1:end);
        sim.mass = str2double(s1);
        fprintf('mass is set to %.0f\n',sim.mass); 
    end
    
    % msh 
    
    if(strcmp(key,'msh')==1)
        
        sim.msh_style = line(blanks(1)+1:blanks(2)-1);
        
        % fix
        
        if(strcmp(sim.msh_style,'fix')==1)
            s1 = line(blanks(2)+1:blanks(3)-1);
            sim.nn = 1+sim.dim;
            % file
            if(strcmp(s1,'file')==1)                
                s1 = line(blanks(3)+1:end);
                sfin = ['../input/' s1];
                sfid = fopen(sfin);
                line = fgetl(sfid);
                sim.en = str2double(line);
                M = zeros(sim.en,1+sim.dim);
                for i1=1:sim.en
                    line = fgetl(sfid);
                    M(i1,:) = 1+str2num(line);                    
                end
            end
            % sel
            if(strcmp(s1,'sel')==1)
                s1 = line(blanks(3)+1:end);
                sel = str2double(s1);                
                mesh = DelaunayTri(X(S(in,2:1+S(in,1)),:));
                sim.en = size(mesh.Triangulation,1);
                for i1=1:sim.en
                    t1 = mesh.Triangulation(i1,:);
                    M(i1,:) = sel(t1);
                end
            end
        end
        
        % log
        
        fprintf('mesh is built with %.0f elemens\n',sim.en);
        
        % mshAfter
        
        f_meshAfter();
        
    end
        
    % NN
    
    if(strcmp(key,'nn')==1)
        s1 = line(blanks(1)+1:blanks(2)-1);
        sim.nnRcut = str2double(s1);
        sim.nnUstyle = line(blanks(2)+1:end);
        %
        sim.nnCheckDispSq = ((sim.nnRcut-sim.potRcut)/2)^2;
        % NN
        f_NNupdate(0);
        fprintf('neighbor lists are built\n');
    end
        
    % pot
    
    if(strcmp(key,'pot')==1)
        sim.potName = line(blanks(1)+1:blanks(2)-1);
        s1 = line(blanks(2)+1:blanks(3)-1);
        sim.potRcut = str2double(s1);
        s1 = line(blanks(3)+1:blanks(4)-1);
        sim.potPar(1) = str2double(s1);
        s1 = line(blanks(4)+1:end);
        sim.potPar(2) = str2double(s1);
        % lj
        if(strcmp(sim.potName,'lj')==1)
            sim.potReq = 2^(1/6)*sim.potPar(2);
            f_pot_lj(0,sim.potPar(1),sim.potPar(2));
            sim.hpot = @f_pot_lj;
        end
        fprintf('potential is set to %s with rCut %.4f and parameters',sim.potName,sim.potRcut);
        for i1=1:numel(sim.potPar)
            fprintf(' %.4f',sim.potPar(i1));
        end
        fprintf('\n');
    end
        
    % scr
    
    if(strcmp(key,'scr')==1)
        s1 = line(blanks(1)+1:end);
        sim.scrUfreq = str2double(s1);
        fprintf('screen update frequency is set to %.0f\n',sim.scrUfreq);
    end
    
    % sel
    
    if(strcmp(key,'sel')==1)  
        % S S0
        if(~exist('S0','var'))
            S = [];
            S0 = (1:sim.an)';
        end  
        % id style
        s1 = line(blanks(1)+1:blanks(2)-1);
        id = str2double(s1);
        style = line(blanks(2)+1:blanks(3)-1);    
        % bnd
        if(strcmp(style,'bnd')==1)
            s1 = line(blanks(3)+1:blanks(4)-1);
            in = str2double(s1);
            dof = line(blanks(4)+1:blanks(5)-1);  
            bnd = str2num(line(blanks(6)+1:end));
            l = f_sel_bnd(X(S(in,2:1+S(in,1)),:),dof,bnd);
        end
        % file
        if(strcmp(style,'file')==1)
            s1 = line(blanks(3)+1:blanks(4)-1);
            sfin = ['../input/' s1 '.txt'];
            sfid = fopen(sfin);
            s1 = fgetl(sfid);
            ll = str2double(s1);
            l = zeros(ll,1);
            for i1=1:ll
                s1 = fgetl(sfid);
                ll(i1) = 1+str2double(s1);                
            end
        end   
        % grd
        if(strcmp(style,'grd')==1)            
            s1 = line(blanks(3)+1:blanks(4)-1);
            in = str2double(s1);
            s1 = line(blanks(4)+1:end); 
            ul = str2num(s1);
            l = f_sel_grd(X(S(in,2:1+S(in,1)),:),ul);
        end  
        % ID
        if(strcmp(style,'ID')==1)
            s1 = line(blanks(3)+1:end);
            l = 1+str2num(s1);
            l = l';
        end  
        % rad
        if(strcmp(style,'rad')==1)     
            s1 = line(blanks(3)+1:blanks(4)-1);
            in = str2double(s1);
            s1 = line(blanks(4)+1:blanks(5)-1); 
            sel = str2double(s1);    
            s1 = line(blanks(5)+1:end);
            rad = str2double(s1);
            l = f_sel_rad(X(S(in,2:1+S(in,1)),:),X(S(sel,2:1+S(sel,1)),:),rad);
        end   
        % subtract
        if(strcmp(style,'subtract')==1) 
            s1 = line(blanks(3)+1:blanks(4)-1);
            from = str2double(s1);
            s1 = line(blanks(4)+1:end); 
            to = str2double(s1);    
            l = setdiff(S(from,2:1+S(from,1),:),S(to,2:1+S(to,1),:));
        end   
        % sur
        if(strcmp(style,'sur')==1)
            sur = line(blanks(3)+1:blanks(4)-1);
            s1 = line(blanks(4)+1:end); 
            dep = str2double(s1);   
            l = f_sel_sur(X,sur,dep);            
        end   
        % union
        if(strcmp(style,'union')==1) 
            s1 = line(blanks(3)+1:blanks(4)-1);
            first = str2double(s1);
            s1 = line(blanks(4)+1:end); 
            second = str2double(s1);    
            l = union(S(first,2:1+S(first,1),:),S(second,2:1+S(second,1),:));
        end 
        % add to S
        l = unique(l);
        ll = numel(l);
        tocat = [ll; l; zeros(sim.an-ll,1)];
        S = cat(2,S,tocat);
        % log        
        fprintf('selection %.0f is bulit with %.0f atoms\n',id,S(1,id));
    end   
    
    % typ
    
    if(strcmp(key,'typ')==1)
        % TYP
        % should be compatible w/ meshAfter
        s1 = line(blanks(1)+1:blanks(2)-1);
        sel = str2double(s1);
        s1 = line(blanks(2)+1:end);
        typ = str2double(s1);
        TYP(S(sel,2:1+S(sel,1))) = typ;        
        % FW
        % should be 1 by default
        FW = ones(sim.an,1);
        FW(TYP==5) = 0;
        for i1=1:sim.en
            psmp = PSMP(i1);
            an = sum(IN==i1);
            FW(psmp) = an;
        end
        % MASS
        % should be 1 by default
        MASS = repmat(sim.mass,sim.an,1);
        MASS(TYP==3 || TYP==4 || TYP==5) = 0;
        for i1=1:sim.en
            nodes = M(i1,:);
            nn = numel(nodes);
            an = sum(IN==i1);
            elementWeight = an*sim.mass;
            MASS(nodes) = elementWeight/nn;
        end
        % log        
        fprintf('selection %.0f is set to type %.0f\n',sel,typ);
        fprintf('atom types are %.0f %.0f %.0f %.0f %.0f\n',sum(TYP==1),sum(TYP==2),sum(TYP==3),sum(TYP==4),sum(TYP==5));
    end
    
    % X0
    
    if(strcmp(key,'x0')==1)
        sim.x0Style = line(blanks(1)+1:blanks(2)-1);
        % file
        if(strcmp(sim.x0Style,'file')==1)
            sim.x0fi = line(blanks(2)+1:end);
            fin2 = ['../input/' sim.x0fi];
            fid2 = fopen(fin2);
            line = fgetl(fid2);
            sim.an = str2double(line);
            X = zeros(sim.an,3);
            for i1=1:sim.an
                line = fgetl(fid2);
                X(i1,:) = str2num(line);
            end
        end
        % uni (1D)
        if(strcmp(sim.x0Style,'uni')==1)
            s1 = line(blanks(2)+1:blanks(3)-1);
            r0 = str2double(s1);
            s1 = line(blanks(3)+1:end);
            l = str2double(s1);
            X = f_gen_1D_uni(l,r0);
        end
        % hex (2D)
        if(strcmp(sim.x0Style,'hex')==1)
            s1 = line(blanks(2)+1:blanks(3)-1);
            r0 = str2double(s1);
            s1 = line(blanks(3)+1:blanks(4)-1);
            lx = str2double(s1);
            s1 = line(blanks(4)+1:end);
            ly = str2double(s1);
            X = f_gen_2D_hex(lx,ly,r0);
        end      
        %
        sim.an = size(X,1);
        X0 = X;
        FW = ones(sim.an,1);
        MASS = repmat(sim.mass,sim.an,1);
        TYP = repmat(2,sim.an,1);
        V = zeros(sim.an,3);
        % log        
        fprintf('initial configuration is built with %.0f atoms\n',sim.an);
    end
    
    % run
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    
    if(strcmp(key,'run')==1)
        sim.solName = line(blanks(1)+1:blanks(2)-1);
        s1 = line(blanks(2)+1:blanks(3)-1);
        sim.steptot = str2double(s1);
        s1 = line(blanks(3)+1:blanks(4)-1);
        sim.ittot = str2double(s1);
        s1 = line(blanks(4)+1:end);
        sim.dt = str2double(s1);
        % vv
        if(strcmp(sim.solName,'vv')==1)
            sim.hsol = @f_sol_vv;
        end
        
        % initial trajectory
        col = zeros(sim.an,3);
        col(TYP==1,:) = repmat([1 0 0],sum(TYP==1),1);
        col(TYP==3,:) = repmat([0 0 1],sum(TYP==3),1);
        col(TYP==4,:) = repmat([0 1 0],sum(TYP==4),1);
        col(TYP==5,:) = repmat([.5 .5 .5],sum(TYP==5),1);
        figure(1);
        scatter3(X(:,1),X(:,2),X(:,3),50,col,'filled');
        axis equal; view(vi); grid off;
        
        % first crack point
        if(sim.isXMMM)
            % manual start: first point in C
            Cn = 1;
            C(Cn,:) = [1 1 58 93]; % element / status / coor
            % manual end
        end
        
        % efk
        f_energyForce([1 1]);
        
        % log
        if(strcmp(sim.solName,'vv')==1)
            fprintf('run by solver velocity-verlet for %.0f steps of %.0f iterations with timestep %.4f\n',sim.steptot,sim.ittot,sim.dt);
            fprintf('--------\n');
        end
        
        % itloop
        sim.itloop = 0;
        while(~stop)
            % sim.itloop
            sim.itloop = sim.itloop+1;
            % NN
            f_NNupdate();
%             % crack track
%             % C:: 1: element / 2: status (0: interoir 1: edge) / 3 & 4: atom IDs
%             if(sim.isXMMM)
%                 stretched = f_buildB();                    
%                 if(~isempty(stretched))
%                     tipIn = IN(stretched(1));
%                     tipX = mean(X(stretched,:));
%                     if(C(Cn,2)==1) % if last point is on the edge (status 1)
%                         Cn = Cn+1; % add new point 
%                         C(Cn,1) = tipIn; 
%                         C(Cn,2) = 0; % status interior
%                         C(Cn,3:4) = stretched;
%                     else % if last point is interior (status 0)
%                         C(Cn,3:4) = stretched; % update point
%                         % check if in new element
%                         currentIn = C(Cn,1);
%                         if(tipIn~=currentIn)  % if in new element
%                             C(Cn,2) = 1; % convert last point to edge
%                             Cn = Cn+1; % add new point
%                             C(Cn,1) = tipIn;
%                             C(Cn,2) = 1; % status edge
%                             C(Cn,3:4) = stretched;                           
%                         end
%                     end    
%                 end
%             end
            % EF
%             if(XFEM && Cn>1)
                tic;
                % EF (from dr. song)
                EF = zeros(sim.an,sim.nn);
                for i1=1:sim.an
                    if(TYP(i1)==3 || TYP(i1)==4 || TYP(i1)==5)
                        atom = i1;
                        element = IN(i1);
%                         Ci = find(C(:,1)==element);
%                         if(isempty(Ci)); continue; end;
%                         nodes = M(element,:);
%                         Cs = C(Ci,2);
%                         if(Cs(1)==1 && Cs(2)==0) % if crack tip situtation
%                             Xc = mean(X(C(Ci(1),3:4),:));
%                             Xtip = mean(X(C(Ci(2),3:4),:));
%                             ef = get_EF_2d_tri_tip(X,atom,nodes,Xc',Xtip');
%                         end                        
%                         if(Cs(1)==1 && Cs(2)==1) % if crack cut-through situtation
%                             Xc1 = mean(X(C(Ci(1),3:4),:));
%                             Xc2 = mean(X(C(Ci(2),3:4),:));
%                             ef = get_EF_2D_tri(X,atom,nodes,Xc1',Xc2');
%                         end
%                         EF(atom,:) = t1;                    
                        nodes = M(element,:);                        
                        Xc = [7.77 0 0];
                        Xtip = [7.77 3.36 0];                       
                        ef = get_EF_2D_tri_tip(X,atom,nodes,Xc',Xtip');
                        EF(atom,:) = ef'; 
                    end
                end
                fprintf('enrichment functions are built\n');
                Cn = 2;
                C(Cn,:) = [1 0 77 86];  
                toc;
%             end
            % iterate
            sim.hsol();
            % check stop
            if(sim.itloop==sim.ittot)
                stop = 1;
            end
            % energy
            sim.Ekin = 0.5*(MASS'*sum(V.^2,2));
            sim.Etot = sim.Epot+sim.Ekin;
            % log
            if(sim.itloop==1)
                fprintf('iterations\n');                 
            end
            if(sim.itloop==1 || mod(sim.itloop,sim.scrUfreq)==0 || stop)
                % screen
                fprintf('%10.0f\n',sim.itloop);
                % in-loop trajectory
                figure(100);
                scatter3(X(:,1),X(:,2),X(:,3),50,col,'filled');
                axis equal; view(vi); grid off; drawnow;
            end
        end
        fprintf('--------\n');
    end
            
end

% final trajectory
figure(2);
scatter3(X(:,1),X(:,2),X(:,3),50,col,'filled');
axis equal; view(vi); grid off;

% finalize

fclose all;

% end

fprintf('\nthe end\n\n');

