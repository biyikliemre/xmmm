function X = f_gen_2D_hex(lx,ly,r0)

rx = r0;
ry = sqrt(3)/2*r0;

t1 = ceil(lx/rx)+1;
t2 = ceil(ly/ry)+1;
X = zeros(t1*t2,3);

c1 = 0;
cy = 0;
while(1)
    cy = cy+1;
    ty = (cy-1)*ry;
    if(ty>ly)
        break
    end
    cx = 0;
    while(1)
        cx = cx+1;
        tx = (cx-1)*rx;
        if(mod(cy,2)==0)
            tx = tx+rx/2;
        end
        if(tx>lx)
            break
        end
        c1 = c1+1;
        X(c1,:) = [tx ty 0];
    end
end
X = X(1:c1,:);

