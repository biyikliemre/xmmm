function f_meshAfter()

global FW IN M MASS PSMP SF sim TYP X

% IN
% -1 outside 
% -2 irep
IN = repmat(-1,sim.an,1);
for i1=1:sim.an
    % if irep
    if(~isempty(find(M==i1,1)))
        IN(i1) = -2;
        continue;
    end
    % f_IN
    IN(i1) = f_IN(i1);
end
fprintf('IN is built\n');

% PSMP
% -1 if it is not
PSMP = repmat(-1,sim.en,1);
for i1=1:sim.en
    PSMP(i1) = f_sel_eam(i1);
end
fprintf('primary sampling atoms are determined\n');

% TYP
% should be 2 by default
TYP = repmat(2,sim.an,1);
TYP(IN~=-1) = 5;
TYP(PSMP) = 3;
TYP(unique(M)) = 1;
fprintf('types are built\n');

% SF
% isoparametric: http://www.colorado.edu/engineering/cas/courses.d/IFEM.d/IFEM.Ch16.d/IFEM.Ch16.pdf
SF = zeros(sim.an,sim.nn);
for i1=1:sim.an
    if(TYP(i1)==3 || TYP(i1)==4 || TYP(i1)==5)
        Xatom = X(i1,:);
        element = IN(i1);
        nodes = M(element,:);
        Xnodes = X(nodes,:);
        A = [ones(1,sim.nn); (Xnodes(:,1:sim.dim))'];;
        B = [1; (Xatom(1:sim.dim))'];;
        weights = A\B;
        weights = abs(weights);
        weights(weights<1e-12) = 0;
        SF(i1,:) = weights;
    end
end
fprintf('shape functions are built\n');

% FW
% should be 1 by default
FW = ones(sim.an,1);
FW(TYP==5) = 0;
for i1=1:sim.en
    psmp = PSMP(i1);
    an = sum(IN==i1);
    FW(psmp) = an;
end
fprintf('force weights are built\n');

% MASS
% should be 1 by default
MASS = repmat(sim.mass,sim.an,1);
MASS(TYP==3 | TYP==4 | TYP==5) = 0;
for i1=1:sim.en
    nodes = M(i1,:);
    nn = numel(nodes);
    an = sum(IN==i1);
    elementWeight = an*sim.mass;
    MASS(nodes) = MASS(nodes)+elementWeight/nn;
end
fprintf('masses are built\n');

