function EF=get_EF_2D_tri_tip(X,atom,nodes,Xc,Xtip)
syms ss tt               % material coordinates
syms N1 N2 N3
% X: an array consisting of material coordinates of atoms X=[X1 Y1; X2 Y2;..]

% atom: The index of atom

% nodes: nodes of interpolating atoms for the element
%               nodes(1)== (s=+1)&(t=0)
%               nodes(2)== (s=0)&(t=+1)
%               nodes(3)== (s=0)&(t=0)

% Xc : The coordinates of the crack with the element edge
%               Xc(1:2,1)= the coordinates of the intersection

% Xtip : The coordinates of the crack tip
%               Xtip(1:2,1)= the coordinates of the tip


Xatom = X(atom,:);       % an 1*2 array as the material coordinates of the atom
Xnodes = X(nodes,:);     % an 3*2 array as the material coordinates of element nodes

N1=sym('ss');
N2=sym('tt');
N3=sym('1-ss-tt');


[Ntheta,Nr]=get_theta_r_tri(Xc,Xtip);
theta_line=get_theta(Xc,Xtip);

h=solve(Xc(1)-N1*Xnodes(1,1)-N2*Xnodes(2,1)-N3*Xnodes(3,1),Xc(2)-N1*Xnodes(1,2)-N2*Xnodes(2,2)-N3*Xnodes(3,2));
h.ss=double(h.ss);
h.tt=double(h.tt);
ksi_c=h.ss(h.ss>=0 & h.ss<=1);
eta_c=h.tt(h.tt>=0 & h.tt<=1);

h=solve(Xtip(1)-N1*Xnodes(1,1)-N2*Xnodes(2,1)-N3*Xnodes(3,1),Xtip(2)-N1*Xnodes(1,2)-N2*Xnodes(2,2)-N3*Xnodes(3,2));
h.ss=double(h.ss);
h.tt=double(h.tt);
ksi_tip=h.ss(h.ss>=0 & h.ss<=1);
eta_tip=h.tt(h.tt>=0 & h.tt<=1);

h=solve(Xatom(1)-N1*Xnodes(1,1)-N2*Xnodes(2,1)-N3*Xnodes(3,1),Xatom(2)-N1*Xnodes(1,2)-N2*Xnodes(2,2)-N3*Xnodes(3,2));
h.ss=double(h.ss);
h.tt=double(h.tt);
ksi=h.ss(h.ss>=0 & h.ss<=1);
eta=h.tt(h.tt>=0 & h.tt<=1);



rx=sqrt((ksi-ksi_tip)^2+(eta-eta_tip)^2);
thetax=get_theta(Xtip,[ksi eta])-theta_line;

 if thetax>pi
    thetax=thetax-2*pi;
end
if thetax<-pi
    thetax=thetax+2*pi;
end

sai_1=sqrt(rx)*sin(thetax/2)-sqrt(Nr(1))*sin(Ntheta(1)/2);
sai_2=sqrt(rx)*sin(thetax/2)-sqrt(Nr(2))*sin(Ntheta(2)/2);
sai_3=sqrt(rx)*sin(thetax/2)-sqrt(Nr(3))*sin(Ntheta(3)/2);


N1=ksi;
N2=eta;
N3=1-ksi-eta;


EF=[N1*sai_1; N2*sai_2; N3*sai_3]; % a 1*3 matrix

end


function theta=get_theta(X1,X2)

if(X2(1)-X1(1))>=0
    if(X2(2)-X1(2))>=0
        region=1;  % frist quarter
    else
        region=4;  % forth quarter
    end
else
    if(X2(2)-X1(2))>=0
        region=2;  % second quarter
    else
        region=3;  % third quarter
    end
end


theta_t=atan((X2(2)-X1(2))/(X2(1)-X1(1)));

if(region==1)
    theta=theta_t;
elseif(region==2)
    theta=theta_t+pi;
elseif(region==3);
    theta=theta_t+pi;
elseif(region==4);
    theta=theta_t+2*pi;
end
   


return
end

function [theta,r]=get_theta_r_tri(ss,stip)
theta=zeros(1,3);
r=zeros(1,3);
r(1)=sqrt((stip(1,1)-(+1))^2+(stip(2,1)-(0))^2);
r(2)=sqrt((stip(1,1)-(0))^2+(stip(2,1)-(+1))^2);
r(3)=sqrt((stip(1,1)-(0))^2+(stip(2,1)-(0))^2);

theta_line=get_theta(ss,stip);

theta(1)=get_theta(stip,[+1,0])-theta_line;
theta(2)=get_theta(stip,[0,+1])-theta_line;
theta(3)=get_theta(stip,[0,0])-theta_line;


for i=1:3
    if theta(i)>pi
        theta(i)=theta(i)-2*pi;
    end
    if theta(i)<-pi
        theta(i)=theta(i)+2*pi;
    end
end


end