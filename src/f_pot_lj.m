function [E,F,K] = f_pot_lj(opt,I1,I2,I3,I4,I5)

persistent epsilon sigma A B; 

if(opt==0) % initialize 
    
    epsilon = I1;
    sigma = I2;
    A = 4*epsilon*sigma^12;
    B = 4*epsilon*sigma^12;
    
else
    
    x = I1;
    Xnn = I2;
    nnn = size(Xnn,1);
    V = I3;
    D = I4;
    dim = I5;
    
    efFlag = opt;
    if(efFlag(1)) 
        E = zeros(nnn,1);
    else
        E = [];
    end
    if(efFlag(2)) 
        F = zeros(nnn,3);
    else
        F = [];
    end
               
    for i2=1:nnn
        v = V(i2,:);
        d = D(i2,:);
        % energy
        if(efFlag(1))
            E(i2) = A/d^12-B/d^6;
        end
        % force
        if(efFlag(2))
            fMag = -6*B/d^8+12*A/d^14;
            F(i2,:) = fMag.*v;
        end        
    end
    
end
        
        