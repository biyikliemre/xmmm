function f_sol_vv()

global C D EF F IN M MASS SF sim TYP V X X0

D(TYP<=2,:) = sim.dt.*V(TYP<=2,:)+(0.5*sim.dt^2).*bsxfun(@rdivide,F(TYP<=2,:),MASS(TYP<=2));
X(TYP<=2,:) = X(TYP<=2,:)+D(TYP<=2,:);

if(sim.isMMM)
    for i1=1:sim.an
        if(TYP(i1)==1 || TYP(i1)==2); continue; end;
        element = IN(i1);
        irep = M(element,:);
        Direp = D(irep,:);
        X(i1,:) = X(i1,:)+SF(i1,:)*Direp;
    end
end

if(sim.isXMMM)
    DG = bsxfun(@minus,X,X0);
    for i1=1:sim.an
        if(TYP(i1)==3 || TYP(i1)==4 || TYP(i1)==5)
            atom = i1;
            element = IN(i1);
            Ci = find(C(:,1)==element);
            if(isempty(Ci)); continue; end;
            nodes = M(element,:);
            Cs = C(Ci,2);
            if(Cs(1)==1 && Cs(2)==0) % if crack tip situtation
                Xc = mean(X(C(Ci(1),3:4),:));
                Xtip = mean(X(C(Ci(2),3:4),:));
                t1 = X(C(Ci(1),3),:)-X(C(Ci(1),4),:);
                t2 = X(C(Ci(2),3),:)-X(C(Ci(2),4),:);
                jump = [t1; t2];
                Ucm = mean(DG(C(Ci(1),3:4),:));
                [DG,X] = get_interpolated_2d_tri_tip(DG,X,X0,M,element,nodes,Xc',Xtip',jump,Ucm,EF);                                
            end                        
            if(Cs(1)==1 && Cs(2)==1) % if crack cut-through situtation
                Xc = mean(X(C(Ci(1),3:4),:));
                Xtip = mean(X(C(Ci(2),3:4),:));
                t1 = X(C(Ci(1),3),:)-X(C(Ci(1),4),:);
                t2 = X(C(Ci(2),3),:)-X(C(Ci(2),4),:);
                jump = [t1; t2];
                Ucm = mean(DG(C(Ci(1),3:4),:));
                [DG,X] = get_interpolated_2d_tri(DG,X,X0,nodes,element,nodes,Xc',jump,Ucm,EF);                                
            end
        end
    end
end

FP = F;

f_energyForce([1 1]);

V = V+(0.5*sim.dt).*bsxfun(@rdivide,F+FP,MASS);

