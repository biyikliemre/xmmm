function [l,ll] = f_sel_bnd(Xin,dof,bnd)

dofFlag = [0 0 0];
bnd6 = zeros(6,1);
c1 = 0;
if(strfind(dof,'x')==1); dofFlag(1) = 1; bnd6(1:2)=bnd(c1+1:c1+3); c1=c1+2; end;
if(strfind(dof,'y')==1); dofFlag(2) = 1; bnd6(3:4)=bnd(c1+1:c1+3); c1=c1+2; end;
if(strfind(dof,'z')==1); dofFlag(3) = 1; bnd6(5:6)=bnd(c1+1:c1+3); c1=c1+2; end;

l = (1:size(X,1))';
Xret = Xin;
for i1=1:3
    if(dofFlag(i1)==1)
        bndlo = bnd(2*i1-1);
        bndhi = bnd(2*i1);
        out = Xret(:,i1)<bndlo & Xret(:,i1)>bndhi;
        Xret(out,:) = [];
        l(out,:) = [];
    end
end

if(nargout>1)
    ll = numel(l);
end

