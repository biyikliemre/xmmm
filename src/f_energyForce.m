function f_energyForce(efFlag)

global E F FF FW IN M NN PSMP SF sim TYP X

% init
  
if(efFlag(1))
    E = zeros(sim.an,1);
end
if(efFlag(2))
    F = zeros(sim.an,3);
end

for i1=1:sim.an
    a1X = X(i1,:); % atom coordinate
    a1nnn = NN(i1,1); % atom nn #
    for i2=1:a1nnn        
        a2 = NN(i1,1+i2);
        
        if(a2<i1); continue; end;
        
        if(TYP(i1)==5 && TYP(a2)==5); continue; end;
        
        a2X = X(a2,:);
        v = bsxfun(@minus,a1X,a2X);
        d = norm(v);
        
        if(d>sim.potRcut); continue; end;
       
        [e,f] = sim.hpot(efFlag,a1X,a2X,v,d,sim.dim);
              
        if(efFlag(1)) 
            E(i1) = E(i1)+e/2;
            E(a2) = E(a2)+e/2;
        end  
        
        if(efFlag(2))            
            fw = (FW(i1)+FW(a2))/2;
            f = fw.*f;            
            F(i1,:) = F(i1,:)+f;
            F(a2,:) = F(a2,:)-f;
        end
                
    end    
end

% MMM

if(sim.isMMM)
    for i1=1:sim.en        
        % energy
        if(efFlag(1))
            psmp = PSMP(i1);
            nsmp = TYP==5 & IN==i1;
            E(nsmp) = E(psmp);
        end
        % force
        if(efFlag(2))
            irep = M(i1,:);
            gho = (TYP==3 | TYP==4 | TYP==5) & IN==i1;
            F(irep,:) = F(irep,:)+SF(gho,:)'*F(gho,:);
            F(gho,:) = 0;
        end
    end
end

% energy

if(efFlag(1))
    sim.Epot = sum(E);
end
 
% fix/force

if(efFlag(2))
    fix = FF(:,1)==1;
    F(fix,:) = F(fix,:).*~FF(fix,2:4);
    frc = FF(:,1)==2;
    F(frc,:) = F(frc,:)+FF(frc,2:4); % if to add
%     F(frc,:) = FF(frc,2:4); % if to set
end

