function X = f_gen_1D_uni(l,r0)

rx = r0;

t1 = ceil(l/rx)+1;
X = zeros(t1,3);

c1 = 0;
cx = 0;
while(1)
    cx = cx+1;
    tx = (cx-1)*rx;
    if(tx>l)
        break
    end
    c1 = c1+1;
    X(c1,:) = [tx 0 0];
end
X = X(1:c1,:);

