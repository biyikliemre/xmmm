function [l,ll] = f_sel_grd(Xin,ul)

ax = zeros(1,6);
ax(1:2:5) = min(Xin);
ax(2:2:6) = max(Xin);
len = ax(2:2:6)-ax(1:2:5);  

t1 = round(len./ul)+1; % number of atoms to be selected in each dimension
t1(t1==1) = t1(t1==1)+1; % t1 cannot be 1
t2 = prod(t1); % total number of atoms to be selected
l = zeros(t2,1); % list of selected atoms
aul = len./(t1-1); % actual unit length in each dimension
aul(isnan(aul)) = 0;
 
t4 = zeros(1,3); % target position
c1 = 0;
for i1=1:t1(1)
    t4(1) = ax(1)+aul(1)*(i1-1);
    for i2=1:t1(2)
        t4(2) = ax(3)+aul(2)*(i2-1);
        for i3=1:t1(3)
            t4(3) = ax(5)+aul(3)*(i3-1);
            t5 = pdist2(t4,X);
            t6 = find(t5==min(t5),1);
            c1 = c1+1;
            l(c1) = t6;
        end
    end
end   

l = unique(l);
if(nargout>1)
    ll = numel(l);
end

