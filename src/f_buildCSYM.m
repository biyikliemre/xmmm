function f_buildCSYM()

global CSYM NN sim X

CSYM(:) = 0;
V = zeros(sim.an,3);

for i1=1:sim.an
    a1X = X(i1,:); % atom coordinate
    a1nnn = NN(i1,1); % atom nn #
    for i2=1:a1nnn        
        a2 = NN(i1,1+i2);        
        if(a2<i1); continue; end;        
        a2X = X(a2,:);
        v = a2X-a1X;
        d = norm(v);        
        if(d>1.1*sim.potReq); continue; end;          
        V(i1,:) = V(i1,:)+v;
        V(a2,:) = V(a2,:)-v;                
    end    
end

for i1=1:sim.an
    CSYM = sqrt(sum(V.^2,2));
end

