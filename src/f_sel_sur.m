function [l,ll] = f_sel_sur(X,sur,dep)

% initialize

l1 = size(X,1);
l = zeros(l1,1);

ax = zeros(1,6);
ax(1:2:5) = min(X);
ax(2:2:6) = max(X);
len = ax(2:2:6)-ax(1:2:5);

% options

sur = [sur(1:2:end-1); sur(2:2:end)]';

% calculate

c1 = 0;
% x-
if(ismember('x-',sur,'rows'))
    t1 = abs(X(:,1)-ax(1));
    t2 = find(t1<=dep);
    t3 = numel(t2);
    l(c1+1:c1+t3) = t2;
    c1 = c1+t3;
end
% x+
if(ismember('x+',sur,'rows'))
    t1 = abs(X(:,1)-ax(2));
    t2 = find(t1<=dep);
    t3 = numel(t2);
    l(c1+1:c1+t3) = t2;
    c1 = c1+t3;
end
% y-
if(ismember('y-',sur,'rows'))
    t1 = abs(X(:,2)-ax(3));
    t2 = find(t1<=dep);
    t3 = numel(t2);
    l(c1+1:c1+t3) = t2;
    c1 = c1+t3;
end  
% y+
if(ismember('y+',sur,'rows'))
    t1 = abs(X(:,2)-ax(4));
    t2 = find(t1<=dep);
    t3 = numel(t2);
    l(c1+1:c1+t3) = t2;
    c1 = c1+t3;
end
% z-
if(ismember('z-',sur,'rows'))
    t1 = abs(X(:,3)-ax(5));
    t2 = find(t1<=dep);
    t3 = numel(t2);
    l(c1+1:c1+t3) = t2;
    c1 = c1+t3;
end
% z+
if(ismember('z+',sur,'rows'))
    t1 = abs(X(:,3)-ax(6));
    t2 = find(t1<=dep);
    t3 = numel(t2);
    l(c1+1:c1+t3) = t2;
    c1 = c1+t3;
end

l = l(1:c1);
l = unique(l);
if(nargout>1)
    ll = numel(l);
end

