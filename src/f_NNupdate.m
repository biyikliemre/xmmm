function f_NNupdate(opt)

global NN sim X XM

% first

if(nargin>0)
    build = 1;
end

% auto 

if(nargin<1 && strcmp(sim.nnUstyle,'auto')==1)
    build = 0;
    U = X-XM;
    UM = sum(U.^2,2);
    if(~isempty(find(UM>sim.nnCheckDispSq,1)))
        build = 1;
    end    
end

% build

if(build)
    %
    XM = X;
    %
    distance = pdist2(X,X);
    distance(distance==0) = Inf;
    t2 = sum(sum(distance<=sim.potRcut));
    NN = zeros(sim.an,1+t2);
    for i1=1:sim.an
        t3 = find(distance(i1,:)<=sim.potRcut);
        t4 = numel(t3);
        NN(i1,1) = t4;
        NN(i1,2:1+t4) = t3;
    end
end

