function [l,ll,d] = f_sel_rad(Xin,X,rad)

l1 = size(X,1);

t1 = mean(X,1);
t2 = pdist2(t1,Xin);
t3 = find(t2<=rad);
t4 = numel(t3);
l2 = 2*t4;

l = zeros(l1,l2);
if(nargout>1)
    ll = zeros(l1,1);
    if(nargout>1)
        d = zeros(l1,l2);
    end
end

c1 = 0;
for i1=1:l1
    t1 = X(i1,:);
    t2 = pdist2(t1,Xin);
    t2(t2==0) = Inf;
    t3 = find(t2<=rad);
    t4 = numel(t3);
    if(t4==0)
        qwer;
    end
    
    if(t4>l2)
        l = [l zeros(l1,l2)];
        l2 = 2*l2;
    end
    
    l(i1,1:t4) = t3;
    if(nargout>1)
        ll(i1) = t4;
        if(nargout>2)
            d(i1,1:t4) = t2(t3);
        end
    end
    
    if(t4>c1)
        c1 = t4;
    end
end

l = l(:,1:c1);
if(nargout>2)
    d = d(:,1:c1);
end

