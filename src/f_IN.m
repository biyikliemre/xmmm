function element = f_IN(atom)

global sim M X
element = -1;

switch sim.dim
    case 1
        for i1=1:sim.en
            nodes = M(i1,:);            
            if(prod([X(atom)-X(nodes(1)) X(atom)-X(nodes(2))])<0)
                element = i1;
                break;
            end
        end
    case 2
        % http://www.blackpawn.com/texts/pointinpoly/default.html
        for i1=1:sim.en
            angles = zeros(1,3);
            t1 = M(i1,:);
            t2 = X(t1,:);
            t3 = bsxfun(@minus,t2,X(atom,:));
            c1 = 0;
            for i2=1:2
                t4 = t3(i2,:);
                for i3=i2+1:3
                    t5 = t3(i3,:);
                    c1 = c1+1;
                    angles(c1) = acos(dot(t4,t5)/norm(t4)/norm(t5));
                end
            end
            t6 = sum(angles);
            t7 = t6-2*pi;
            if(abs(t7)<1e-6)
                element = i1;
                break;
            end
        end
    case 3
        % to do
end

