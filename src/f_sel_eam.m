function l = f_sel_eam(element)

global IN M X

nodes = M(element,:);
mp = mean(X(nodes,:));
inside = find(IN==element);
Xinside = X(inside,:);
distance = pdist2(mp,Xinside);
closest = find(distance==min(distance),1);
l = inside(closest);

