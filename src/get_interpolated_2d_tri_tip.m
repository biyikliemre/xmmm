function [U,x] = get_interpolated_2d_tri_tip(U,x,X,conec,elem_enrich,nodes,Xc,Xtip,jump,Ucm,EF)
% Xc : The coordinates of the crack with the element edge
%               Xc(1:2,1)= the coordinates of the intersection
% Xtip : The coordinates of the crack tip
%               Xtip(1:2,1)= the coordinates of the tip
% nodes is an 1*n array consisting node labels inside the element
% jump is a 2*2 array consisting of: first row is jump in displacement for x direction and second row
% is jump for the displacement for the y direction and first column is
% relatedt to jumps in intersection of crack and edge and second column is
% the jump at middle of crack tip and crack intersection
% Ucm is a 2*1 the mean of displacement at both sides of the crack at
%crack intersection by edge. Ucm(1,1) for the Ux and Ucm(2,1) for the Uy   
    irep = conec(elem_enrich,:); % interpolating rep-atom % irep is an 1*3 array consisting element node numbers;
    Unodes = U(irep);
    
    % Unodes: an 4*2 array consisting of nodal displaceements U=[U1 V1
    %                                                            U2 V2
    %                                                            U3 V3
    %                                                            U4 V4]
    
    q_ux=get_q(Unodes(:,1),X,irep,Xc,Xtip,jump(1,:),Ucm(1));
    q_uy=get_q(Unodes(:,2),X,irep,Xc,Xtip,jump(2,:),Ucm(2));
    
    for i2=1:length(nodes)
         i_node=nodes(i2);
        U(i_node,:)=U(i_node,:)+EF(elem_enrich,:)*[q_ux q_uy];
        x(i_node,:)=X(i_node,:)+U(i_node,:);
    end
end

function q = get_q(Unodes,X,nodes,Xc,Xtip,jump,Ucm)
% q: an 1*3 array consisting enrichment variables for U or V (depends on
% input)
% nodes: nodes of interpolating atoms for the element
%               nodes(1)== (s=+1)&(t=0)
%               nodes(2)== (s=0)&(t=+1)
%               nodes(3)== (s=0)&(t=0)


% X: an array consisting of material coordinates of atoms X=[X1 Y1; X2 Y2;..]

% Xc : The coordinates of the crack with element edges
%               Xc(1:2,1)= the coordinates of first intersection
% Xtip : The coordinates of the crack tip
%               Xtip(1:2,1)= the coordinates of the tip
% Unodes: an 3*1 array consisting of nodal displaceements U=[U1;U2;U3] or U=[V1;V2;V3]
% jump is a 1*2 array consisting of jump at the intersection of crack with
% the edge and jump at middle between crack tip and edge; (jump for Ux or Uy depends on input)
% Ucm is  the mean of displacement at both sides of the crack at
% the intersection of crack with the edge.(depends on input in could be for Ux or Uy


Xnodes = X(nodes,:);   % an 4*2 array as the material coordinates of element nodes
N1=sym('ss');
N2=sym('tt');
N3=sym('1-ss-tt');

[Ntheta,Nr]=get_theta_r_tri(Xc,Xtip);

h=solve(Xc(1)-N1*Xnodes(1,1)-N2*Xnodes(2,1)-N3*Xnodes(3,1),Xc(2)-N1*Xnodes(1,2)-N2*Xnodes(2,2)-N3*Xnodes(3,2));
h.ss=double(h.ss);
h.tt=double(h.tt);
ksi_c=h.ss(h.ss>=0 & h.ss<=1);
eta_c=h.tt(h.tt>=0 & h.tt<=1);
h=solve(Xtip(1)-N1*Xnodes(1,1)-N2*Xnodes(2,1)-N3*Xnodes(3,1),Xtip(2)-N1*Xnodes(1,2)-N2*Xnodes(2,2)-N3*Xnodes(3,2));
h.ss=double(h.ss);
h.tt=double(h.tt);
ksi_tip=h.ss(h.ss>=0 & h.ss<=1);
eta_tip=h.tt(h.tt>=0 & h.tt<=1);
ksi_m=0.5*(ksi_1+ksi_2);
eta_m=0.5*(eta_1+eta_2);

AA=zeros(3,3);
BB=zeros(3,1);

[AA(1,:),BB(1)]=get_AA_jump([ksi_c eta_c],[ksi_tip eta_tip],jump(1),Ntheta,Nr);
[AA(2,:),BB(2)]=get_AA_jump([ksi_m eta_m],[ksi_tip eta_tip],jump(2),Ntheta,Nr);
[AA(3,:),BB(3)]=get_AA_Ucm([ksi_c eta_c],[ksi_tip eta_tip],Ucm,Unodes,Ntheta,Nr);

q=AA\BB;
end
function [AA,BB]=get_AA_jump(sc,stip,jump,Ntheta,Nr)

syms s t
syms theta r
B=sqrt(r)*sin(theta/2);
N1=s;
N2=t;
N3=1-s-t;

ksi=sc(1);
eta=sc(2);
rx=sqrt((ksi-(stip(1)))^2+(eta-stip(2))^2);
thetax_plus=-pi;
thetax_minus=+pi;

BB=jump;
AA(1,1) =subs(subs(N1,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_plus))-(subs(subs(B,r,Nr(1)),theta,Ntheta(1))))-subs(subs(N1,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_minus))-(subs(subs(B,r,Nr(1)),theta,Ntheta(1))));
AA(1,2) =subs(subs(N2,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_plus))-(subs(subs(B,r,Nr(2)),theta,Ntheta(2))))-subs(subs(N2,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_minus))-(subs(subs(B,r,Nr(2)),theta,Ntheta(2))));
AA(1,3) =subs(subs(N3,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_plus))-(subs(subs(B,r,Nr(3)),theta,Ntheta(3))))-subs(subs(N3,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_minus))-(subs(subs(B,r,Nr(3)),theta,Ntheta(3))));
                        

end  

function [AA,BB]=get_AA_Ucm(sc,stip,Ucm,Unodes,Ntheta,Nr)
      
syms s t
syms theta r
B=sqrt(r)*sin(theta/2);
N1=s;
N2=t;
N3=1-s-t;

ksi=sc(1);
eta=sc(2);
rx=sqrt((ksi-(stip(1)))^2+(eta-stip(2))^2);
thetax_plus=-pi;
thetax_minus=+pi;
     BB=Ucm-subs(subs(N1,s,ksi),t,eta)*Unodes(1)-subs(subs(N2,s,ksi),t,eta)*Unodes(2)-subs(subs(N3,s,ksi),t,eta)*Unodes(3);
     AA(1,1) =0.5*subs(subs(N1,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_plus))-(subs(subs(B,r,Nr(1)),theta,Ntheta(1))))+0.5*subs(subs(N1,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_minus))-(subs(subs(B,r,Nr(1)),theta,Ntheta(1))));
     AA(1,2) =0.5*subs(subs(N2,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_plus))-(subs(subs(B,r,Nr(2)),theta,Ntheta(2))))+0.5*subs(subs(N2,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_minus))-(subs(subs(B,r,Nr(2)),theta,Ntheta(2))));
     AA(1,3) =0.5*subs(subs(N3,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_plus))-(subs(subs(B,r,Nr(3)),theta,Ntheta(3))))+0.5*subs(subs(N3,s,ksi),t,eta)*((subs(subs(B,r,rx),theta,thetax_minus))-(subs(subs(B,r,Nr(3)),theta,Ntheta(3))));

end

function theta=get_theta(X1,X2)

if(X2(1)-X1(1))>=0
    if(X2(2)-X1(2))>=0
        region=1;  % frist quarter
    else
        region=4;  % forth quarter
    end
else
    if(X2(2)-X1(2))>=0
        region=2;  % second quarter
    else
        region=3;  % third quarter
    end
end


theta_t=atan((X2(2)-X1(2))/(X2(1)-X1(1)));

if(region==1)
    theta=theta_t;
elseif(region==2)
    theta=theta_t+pi;
elseif(region==3);
    theta=theta_t+pi;
elseif(region==4);
    theta=theta_t+2*pi;
end
   
end
function [theta,r]=get_theta_r_tri(ss,stip)
theta=zeros(1,3);
r=zeros(1,3);
r(1)=sqrt((stip(1,1)-(+1))^2+(stip(2,1)-(0))^2);
r(2)=sqrt((stip(1,1)-(0))^2+(stip(2,1)-(+1))^2);
r(3)=sqrt((stip(1,1)-(0))^2+(stip(2,1)-(0))^2);

theta_line=get_theta(ss,stip);

theta(1)=get_theta(stip,[+1,0])-theta_line;
theta(2)=get_theta(stip,[0,+1])-theta_line;
theta(3)=get_theta(stip,[0,0])-theta_line;


for i=1:3
    if theta(i)>pi
        theta(i)=theta(i)-2*pi;
    end
    if theta(i)<-pi
        theta(i)=theta(i)+2*pi;
    end
end

end

